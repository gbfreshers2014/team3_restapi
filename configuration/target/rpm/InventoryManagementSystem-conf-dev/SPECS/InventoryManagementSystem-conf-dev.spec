Name: InventoryManagementSystem-conf-dev
Version: 1.0.0
Release: SNAPSHOT20140721135617
Summary: InventoryManagementSystem-conf-dev
License: 2012, GwynnieBee
Distribution: Barcode File Upload Service Configuration
Group: com.gwynniebee
autoprov: yes
autoreq: yes
BuildRoot: /home/niraj/Downloads/Skype/rest-template-project/configuration/target/rpm/InventoryManagementSystem-conf-dev/buildroot

%description

%install
if [ -e $RPM_BUILD_ROOT ];
then
  mv /home/niraj/Downloads/Skype/rest-template-project/configuration/target/rpm/InventoryManagementSystem-conf-dev/tmp-buildroot/* $RPM_BUILD_ROOT
else
  mv /home/niraj/Downloads/Skype/rest-template-project/configuration/target/rpm/InventoryManagementSystem-conf-dev/tmp-buildroot $RPM_BUILD_ROOT
fi

ln -s /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT $RPM_BUILD_ROOT/home/gb/conf/InventoryManagementSystem

%files

%attr(755,root,root) /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT
%attr(755,root,root) /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT/log4j.properties
%attr(755,root,root) /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT/nginx/proxy.conf
%attr(755,root,root) /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT/nginx/upstream.conf
%attr(755,root,root) /home/gb/conf/InventoryManagementSystem-1.0.0-SNAPSHOT/inventory.management.service.properties
%attr(755,root,root) /home/gb/conf/InventoryManagementSystem

Name: InventoryManagementSystem-assembly
Version: 1.0.0
Release: SNAPSHOT20140721140247
Summary: InventoryManagementSystem-assembly
License: 2014, GwynnieBee
Distribution: REST template
Group: com.gwynniebee
autoprov: yes
autoreq: yes
BuildRoot: /home/niraj/Downloads/Skype/rest-template-project/assembly/target/rpm/InventoryManagementSystem-assembly/buildroot

%description

%install
if [ -e $RPM_BUILD_ROOT ];
then
  mv /home/niraj/Downloads/Skype/rest-template-project/assembly/target/rpm/InventoryManagementSystem-assembly/tmp-buildroot/* $RPM_BUILD_ROOT
else
  mv /home/niraj/Downloads/Skype/rest-template-project/assembly/target/rpm/InventoryManagementSystem-assembly/tmp-buildroot $RPM_BUILD_ROOT
fi

%files

%dir %attr(755,root,root) /home/gb

%post
/usr/bin/python /home/gb/bin/InventoryManagementSystem-install.py -c /home/gb/conf/InventoryManagementSystem/inventory.management.service.properties

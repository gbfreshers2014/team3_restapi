/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test.entitymanager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All DAO test cases
 * @author Ipsita
 */
@RunWith(Suite.class)
@SuiteClasses({TestAddGetInventoryEntityManager.class, TestModifyInventoryEntityManager_Delete.class, TestTransectionEntityManager.class })
public class AllEntityManagerTest {

}

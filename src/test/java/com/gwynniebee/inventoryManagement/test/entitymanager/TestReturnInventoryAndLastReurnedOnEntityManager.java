/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.test.entitymanager;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gwynniebee.inventoryManagement.Entities.ModifyInventoryEntityManager;
import com.gwynniebee.inventoryManagement.Entities.TransectionEntityManager;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;

/**
 * @author niraj
 */
public class TestReturnInventoryAndLastReurnedOnEntityManager extends TestEntityManagerBase {

    //static DBI dbi = LiquibaseOperations.getDBI();

    @Test
    public void testAssignInventory() {

        // ModifyInventoryDAO d = dbi.open(ModifyInventoryDAO.class);
        Inventory inv = new Inventory();
        inv.setItemId(102);
        inv.setReturnedTo("JTestAdmin");

        // for available item
       // inv.setItemId(101);

        Inventory response = null;
        try {
            response = ModifyInventoryEntityManager.getInstance().returnInventory(inv);
            assertEquals(1, response.getStatus());
            Transections transection = getCurrentlyIssuedTo(inv.getItemId(), 2);
            assertEquals("nkumar", transection.getEmployeeId());
            assertEquals("adminX", transection.getIssuedBy());
            assertEquals(inv.getReturnedTo(), transection.getReturnTo());
          
            
        } catch (Exception e) {
            System.out.println("Exception occured: " + e);
        }

        System.out.println("test finished!");
    }

    static Transections getCurrentlyIssuedTo(int itemId, int status) {
        // TODO Auto-generated method stub
        List<Transections> transection = null;

        try {

            transection = TransectionEntityManager.getInstance().getTransaction(itemId, status);

        } catch (Exception e) {
            System.out.println("Exception at getCurrentlyIssuedTo " + e);
        }
        Assert.assertThat(transection.size(), greaterThanOrEqualTo(1));
        if (status == 1) {
            return transection.get(0);
        }
        return transection.get(transection.size() - 1);
    }

}

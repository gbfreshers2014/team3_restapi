package com.gwynniebee.inventoryManagement.test.entitymanager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.BaseEntityManager;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;
import com.gwynniebee.inventoryManagement.test.dao.TestDAOBase;

public class TestEntityManagerBase {

    public static final Logger LOG = LoggerFactory.getLogger(TestDAOBase.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();
        Class.forName(LiquibaseOperations.getLiquiHelper().driver);
        DBI dbi =
                new DBI(LiquibaseOperations.getLiquiHelper().url, LiquibaseOperations.getLiquiHelper().username,
                        LiquibaseOperations.getLiquiHelper().password);
        BaseEntityManager.setDbi(dbi);

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
    }

    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }
}

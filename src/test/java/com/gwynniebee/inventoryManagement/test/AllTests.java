/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.gwynniebee.inventoryManagement.test.dao.AllDaoTest;
import com.gwynniebee.inventoryManagement.test.dynamicQuery.TestDynamicQuery;
import com.gwynniebee.inventoryManagement.test.entitymanager.AllEntityManagerTest;
import com.gwynniebee.inventoryManagement.test.serverresource.AllServerResourceTest;

/**
 * All test cases.
 * @author Ipsita
 */
@RunWith(Suite.class)
@SuiteClasses({AllDaoTest.class, TestDynamicQuery.class, AllEntityManagerTest.class, AllServerResourceTest.class })
//@SuiteClasses({TestDynamicQuery.class })
public class AllTests {

}

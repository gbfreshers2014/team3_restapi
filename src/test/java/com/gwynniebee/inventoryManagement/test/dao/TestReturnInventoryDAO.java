/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.test.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import com.gwynniebee.inventoryManagement.dao.ModifyInventoryDAO;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;

/**
 * @author niraj
 *
 */
public class TestReturnInventoryDAO extends TestDAOBase {
    
    DBI dbi = LiquibaseOperations.getDBI();
    
    @Test
    public void testReturnInventory(){
        Inventory inv = new Inventory();
        inv.setItemId(102);
        inv.setReturnedTo("JTestAdmin");
        
        ModifyInventoryDAO modifyInventryDAO = dbi.open(ModifyInventoryDAO.class);
        try{
            returnAndVerifyInventory(inv,modifyInventryDAO);
        }catch(Exception e){
            System.out.println("Exception occured: "+e);
        }finally{
            modifyInventryDAO.close();
        }
    }

    /**
     * @param inv
     * @param modifyInventryDAO
     */
    private void returnAndVerifyInventory(Inventory inv, ModifyInventoryDAO modifyInventryDAO) {
        // TODO Auto-generated method stub
        modifyInventryDAO.begin();
        modifyInventryDAO.returnInventory(inv);
        modifyInventryDAO.updateTransReturn(inv);
        modifyInventryDAO.commit();
        
        Inventory response = TestAssignInventoryDAO.getInventoryById(inv.getItemId(), modifyInventryDAO);
        assertEquals(1, response.getStatus());

        Transections transection = TestAssignInventoryDAO.getCurrentlyIssuedTo(inv.getItemId(), 2);
        assertEquals(inv.getReturnedTo(), transection.getReturnTo());
        assertEquals(2, transection.getStatus());
    }

}
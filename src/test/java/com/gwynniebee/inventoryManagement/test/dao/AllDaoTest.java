/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All DAO test cases
 * @author Ipsita
 */
@RunWith(Suite.class)
@SuiteClasses({TestAddGetInventoryDAO.class, TestAddInventoryDAO.class, TestAssignInventoryDAO.class, TestGetInventoryDAO.class,
        TestGetLastIssuedReturnedOnDAO.class, TestDeleteInventoryDAO.class, TestReturnInventoryDAO.class, TestTransectionDAO.class })
public class AllDaoTest {

}

/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.test.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;

/**
 * @author niraj
 *
 */
public class TestGetInventoryDAO extends TestDAOBase {

    @Test
    public void testGetInventory(){
        DBI dbi = LiquibaseOperations.getDBI();
        AddGetInventoryDAO d = dbi.open(AddGetInventoryDAO.class);
        int id = 101;
        Inventory resp = null;
        try{
            resp = d.getInventoryById(id);
        }catch(Exception e){
            System.out.println("exception occured at get "+e);
        }finally{
            d.close();
        }
        assertEquals(101,resp.getItemId());
        assertEquals("dell", resp.getItemName());
        assertEquals("laptop3", resp.getItemType());
        assertEquals("latitude", resp.getDescription());
        assertEquals("niraj", resp.getCreatedBy());
        assertEquals("updatedBy", resp.getUpdatedBy());
        assertEquals(1, resp.getStatus());
        //assertEquals("2014-07-22T10:55:53.000Z", resp.getCreatedOn().toString());
        //assertEquals("2014-07-22T10:55:53.000Z", resp.getUpdatedOn().toString());
    }
}

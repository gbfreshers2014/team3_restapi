/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.test.dao;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import com.gwynniebee.inventoryManagement.dao.TransectionDAO;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;

/**
 * @author niraj
 *
 */
public class TestGetLastIssuedReturnedOnDAO extends TestDAOBase {

   static DBI dbi = LiquibaseOperations.getDBI();
    @Test
    public void testLastAssignment(){
        Transections transection1 = getCurrentlyIssuedTo(16 ,1);
        Transections transection2 = getCurrentlyIssuedTo(9, 2);
        
        assertEquals(1,transection1.getStatus());
        assertEquals("nkumar16", transection1.getEmployeeId());
        assertNull(transection1.getReturnTo());
        
        assertEquals(2,transection2.getStatus());
    }
    
    static Transections getCurrentlyIssuedTo(int itemId, int status) {
        // TODO Auto-generated method stub
        List<Transections> transection = null;
        TransectionDAO transectionDAO = dbi.open(TransectionDAO.class);
        try {
            transectionDAO.begin();
            transection = transectionDAO.getLastIssuedRETURNEDOn(itemId, status);
            transectionDAO.commit();
        } catch (Exception e) {
            System.out.println("Exception at getCurrentlyIssuedTo " + e);
        } finally {
            transectionDAO.close();
        }
        Assert.assertThat(transection.size(), greaterThanOrEqualTo(1));
        if (status == 1) {
            return transection.get(0);
        }
        return transection.get(transection.size()-1);
    }
}
/**
 * Copyright 2014 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;
/**
 * 
 * @author Niraj
 *
 */
public class TestAddInventoryDAO extends TestDAOBase {
    /**
     * Test Case for Add Inventory.
     */
    @Test
    public void testAddInventory() {
        // Thread.sleep(5*1000);
        DBI dbi = LiquibaseOperations.getDBI();
        AddGetInventoryDAO d = dbi.open(AddGetInventoryDAO.class);
        Inventory inv = new Inventory();
        // inv.setItemId(1);
        inv.setItemName("dell");
        inv.setItemType("laptop");
        inv.setDescription("desc");
        // inv.setNamespaceId(1);
        inv.setCreatedBy("nirajkumar");
        inv.setUpdatedBy("1");
        int id = -1;
        try {
            d.begin();
            id = d.addInventory(inv);
            d.commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            d.close();
        }
        assertNotEquals(0, id);
        assertNotEquals(-1, id);
        System.out.println(id);
    }
}
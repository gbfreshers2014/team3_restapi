/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.test.dao;

import static org.junit.Assert.*;

import java.util.List;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.dao.ModifyInventoryDAO;
import com.gwynniebee.inventoryManagement.dao.TransectionDAO;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;

/**
 * @author niraj
 */
public class TestAssignInventoryDAO extends TestDAOBase {

    static DBI dbi = LiquibaseOperations.getDBI();

    @Test
    public void testAssignInventory() {

        ModifyInventoryDAO d = dbi.open(ModifyInventoryDAO.class);
        Inventory inv = new Inventory();
        inv.setIssuedBy("JTestAdmin");
        inv.setIssuedTo("JTestUser");
        inv.setExpectedReturn(new DateTime(204, 8, 15, 10, 59));

        // for available item
        inv.setItemId(101);
        try {
            assignInvntoryAndVerify(inv, d);
        } catch (Exception e) {
            System.out.println("Exception occured: " + e);
        } finally {
            d.close();
        }

        System.out.println("test finished!");
    }

    /**
     * @param inv
     * @param d
     */
    private void assignInvntoryAndVerify(Inventory inv, ModifyInventoryDAO d) {
        d.begin();
        d.assignInventory(inv);
        d.updateTransAssign(inv);
        d.commit();

        Inventory response = getInventoryById(inv.getItemId(), d);
        assertEquals(2, response.getStatus());

        Transections transection = getCurrentlyIssuedTo(inv.getItemId(), 1);
        assertEquals(inv.getIssuedTo(), transection.getEmployeeId());
        assertEquals(inv.getIssuedBy(), transection.getIssuedBy());

        /*
         * DAO cannot convert the expectedReturn to String, so check not
         * possible
         */
        // assertEquals(inv.getExpectedReturn(),
        // transection.getExpectedReturn());
    }

    /**
     * @param itemId
     * @return
     * @return
     */
    static Transections getCurrentlyIssuedTo(int itemId, int status) {
        // TODO Auto-generated method stub
        List<Transections> transection = null;
        TransectionDAO transectionDAO = dbi.open(TransectionDAO.class);
        try {
            transectionDAO.begin();
            transection = transectionDAO.getLastIssuedRETURNEDOn(itemId, status);
            transectionDAO.commit();
        } catch (Exception e) {
            System.out.println("Exception at getCurrentlyIssuedTo " + e);
        } finally {
            transectionDAO.close();
        }
        Assert.assertThat(transection.size(), greaterThanOrEqualTo(1));
        if (status == 1) {
            return transection.get(0);
        }
        return transection.get(transection.size()-1);
    }

    /**
     * @param itemId
     * @param d
     * @return
     */
    static Inventory getInventoryById(int itemId, ModifyInventoryDAO d) {
        // TODO Auto-generated method stub
        Inventory resp = new Inventory();
        AddGetInventoryDAO d2 = dbi.open(AddGetInventoryDAO.class);
        try {
            d2.begin();
            resp = d2.getInventoryById(itemId);
            d2.commit();
        } catch (Exception e) {
            System.out.println("Exception at get " + e);
        } finally {
            d2.close();
        }
        return resp;
    }

}
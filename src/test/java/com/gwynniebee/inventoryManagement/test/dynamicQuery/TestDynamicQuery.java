/**
 * Copyright 2014 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.test.dynamicQuery;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.test.LiquibaseOperations;

/**
 * @author Ipsita
 */
public class TestDynamicQuery {
    public static final Logger LOG = LoggerFactory.getLogger(TestDynamicQuery.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
    }

    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }

    @Test
    public void testDynamicQuery_1() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM INVENTORY WHERE status<=2";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_2() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        List<String> fields = new ArrayList<String>();
        fields.add("itemId");
        fields.add("itemType");
        fields.add("itemName");
        fp.setFields(fields);
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select itemId, itemType, itemName FROM INVENTORY WHERE status<=2";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_3() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        List<String> fields = new ArrayList<String>();
        fields.add("itemId");
        fields.add("itemType");
        fp.setFields(fields);
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select itemId, itemType FROM INVENTORY WHERE status<=2";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_4() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        List<String> fields = new ArrayList<String>();
        fields.add("itemId");
        fp.setFields(fields);
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select itemId FROM INVENTORY WHERE status<=2";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_5() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        List<String> fields = new ArrayList<String>();
        fields.add("itemId");
        fields.add("itemType");
        fields.add("itemName");
        fp.setFields(fields);
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select itemId, itemType, itemName FROM INVENTORY WHERE status=1";
        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_6() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        fp.addCriteria("itemType", "'laptop'");
        fp.addCriteria("itemName", "'dell'");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM INVENTORY WHERE itemName='dell' AND status=1 AND itemType='laptop'";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_7() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        fp.addCriteria("itemType", "'laptop'");
        fp.addCriteria("itemName", "'dell'");
        fp.addCriteria("limit", "10");
        fp.addCriteria("offset", "0");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM INVENTORY WHERE itemName='dell' AND status=1 AND itemType='laptop' LIMIT 10 OFFSET 0";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_8() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        fp.addCriteria("itemType", "'laptop'");
        fp.addCriteria("itemName", "'dell'");
        fp.addCriteria("limit", "10");
        fp.addCriteria("offset", "5");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM INVENTORY WHERE itemName='dell' AND status=1 AND itemType='laptop' LIMIT 10 OFFSET 5";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_9() {
        FilterParams fp = new FilterParams();
        fp.setTable("TRANSACTION");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM TRANSACTION WHERE status=1";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_10() {
        FilterParams fp = new FilterParams();
        fp.setTable("TRANSACTION");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "1");
        fp.addCriteria("employeeId", "'nkumar16'");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM TRANSACTION WHERE employeeId='nkumar16' AND status=1";

        this.validateQuery(query, exQuery);
    }

    @Test
    public void testDynamicQuery_11() {
        FilterParams fp = new FilterParams();
        fp.setTable("TRANSACTION");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "2");
        fp.addCriteria("employeeId", "'nkumar16'");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM TRANSACTION WHERE employeeId='nkumar16' AND status=2";

        this.validateQuery(query, exQuery);

    }

    @Test
    public void testDynamicQuery_12() {
        FilterParams fp = new FilterParams();
        fp.setTable("INVENTORY");
        Map<String, String> criteria = new HashMap<String, String>();
        fp.setCriteria(criteria);
        fp.addCriteria("status", "3");
        String query = fp.buildQuery();
        LOG.info(query);
        String exQuery = "select * FROM INVENTORY WHERE status=3";

        this.validateQuery(query, exQuery);
    }

    private void validateQuery(String query, String expectedQuery) {
        assertEquals(query, expectedQuery);

        this.runQuery(query);
    }

    private void runQuery(String query) {

        // Run the query as well by Liquibase. This will help us determine that
        // query is syntactically correct.
        ResultSet rs = null;
        try {
            rs = LiquibaseOperations.getLiquiHelper().runQuery(query);
        } catch (SQLException e) {
            Assert.assertFalse("Sql Query: [" + query + "], Exception: " + e.getMessage(), true);
        } finally {
            try {
                LiquibaseOperations.getLiquiHelper().releaseRunQueryResources(rs);
            } catch (SQLException e) {
                LOG.error("Unable to release Query resources. Exception: " + e.getMessage());
            }
        }
    }

}

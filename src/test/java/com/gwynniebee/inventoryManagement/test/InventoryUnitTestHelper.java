package com.gwynniebee.inventoryManagement.test;

import org.joda.time.DateTime;

import com.gwynniebee.inventoryManagement.object.Inventory;

public class InventoryUnitTestHelper {

    public static Inventory getInventoryObject(String itemName, String itemType, String description, DateTime createdOn, String createdBy,
            DateTime updatedOn, String updatedBy, int status) {
        Inventory inv = new Inventory();
        inv.setItemName(itemName);
        inv.setItemType(itemType);
        inv.setDescription(description);
        inv.setCreatedOn(createdOn);
        inv.setCreatedBy(createdBy);
        inv.setUpdatedOn(updatedOn);
        inv.setUpdatedOn(updatedOn);
        inv.setStatus(status);
        return inv;
    }

}

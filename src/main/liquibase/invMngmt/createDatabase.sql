CREATE DATABASE  IF NOT EXISTS `test_team3_invMngmt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_team3_invMngmt`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test_team3_invMngmt
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `INVENTORY_STATUS`
--

DROP TABLE IF EXISTS `INVENTORY_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INVENTORY_STATUS` (
  `statusNumber` int(11) NOT NULL,
  `statusDescription` varchar(20) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  KEY `STATUS_NUMBER` (`statusNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INVENTORY_STATUS`
--

LOCK TABLES `INVENTORY_STATUS` WRITE;
/*!40000 ALTER TABLE `INVENTORY_STATUS` DISABLE KEYS */;
INSERT INTO `INVENTORY_STATUS` VALUES (1,'AVAILABLE','2014-07-21 18:46:51','2014-07-21 13:16:51'),(2,'UNAVAILABLE','2014-07-21 18:46:51','2014-07-21 13:16:51'),(3,'DELETED','2014-07-21 18:46:51','2014-07-21 13:16:51');
/*!40000 ALTER TABLE `INVENTORY_STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-28 14:20:39












CREATE DATABASE  IF NOT EXISTS `test_team3_invMngmt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_team3_invMngmt`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test_team3_invMngmt
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TRANSACTION_STATUS`
--

DROP TABLE IF EXISTS `TRANSACTION_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TRANSACTION_STATUS` (
  `statusNumber` int(11) NOT NULL,
  `statusDescription` varchar(20) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  KEY `STATUS_NUMBER` (`statusNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TRANSACTION_STATUS`
--

LOCK TABLES `TRANSACTION_STATUS` WRITE;
/*!40000 ALTER TABLE `TRANSACTION_STATUS` DISABLE KEYS */;
INSERT INTO `TRANSACTION_STATUS` VALUES (1,'ISSUED','2014-07-21 18:46:51','2014-07-21 13:16:51'),(2,'RETURN','2014-07-21 18:46:51','2014-07-21 13:16:51');
/*!40000 ALTER TABLE `TRANSACTION_STATUS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-28 14:20:39











CREATE DATABASE  IF NOT EXISTS `test_team3_invMngmt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_team3_invMngmt`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test_team3_invMngmt
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `INVENTORY`
--


DROP TABLE IF EXISTS `INVENTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INVENTORY` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(50) NOT NULL,
  `itemType` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `namespaceId` int(11) NOT NULL DEFAULT '0',
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(254) NOT NULL,
  `updatedBy` varchar(254) DEFAULT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  KEY `STATUS` (`status`),
  CONSTRAINT `fk_INVENTORY_1` FOREIGN KEY (`status`) REFERENCES `INVENTORY_STATUS` (`statusNumber`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INVENTORY`
--

LOCK TABLES `INVENTORY` WRITE;
/*!40000 ALTER TABLE `INVENTORY` DISABLE KEYS */;
INSERT INTO `INVENTORY` VALUES (1,'dell','laptop','latitude',0,'2014-07-21 18:59:20','nkumar','updatedBy','2014-07-21 13:29:20',1),(2,'dell','laptop2','latitude',0,'2014-07-21 19:04:01','niraj','updatedBy','2014-07-21 13:34:01',1),(3,'dell','laptop3','latitude',0,'2014-07-21 19:19:11','niraj','updatedBy','2014-07-21 13:49:11',1),(4,'dell','laptop3','latitude',0,'2014-07-21 19:19:13','niraj','updatedBy','2014-07-21 13:49:13',1),(5,'dell','laptop3','latitude',0,'2014-07-21 19:38:30','niraj','updatedBy','2014-07-21 14:08:30',1),(6,'dell','laptop3','latitude',0,'2014-07-21 19:49:45','niraj','updatedBy','2014-07-23 07:01:26',3),(7,'acer','mouse','wireless',0,'2014-07-21 19:51:07','niraj','shikha','2014-07-22 10:42:48',1),(8,'dell','laptop3','latitude',0,'2014-07-21 19:51:07','niraj','updatedBy','2014-07-22 10:56:32',1),(9,'modifiedName22','modifiedagain','new desc',0,'2014-07-21 19:51:14','niraj','me3','2014-07-23 05:48:10',1),(10,'dell','laptop3','latitude',0,'2014-07-21 19:54:48','niraj','updatedBy','2014-07-21 14:24:48',1),(11,'dell','laptop3','latitude',0,'2014-07-21 19:55:02','niraj','updatedBy','2014-07-21 14:25:02',1),(12,'dell','laptop4','latitude',0,'2014-07-21 19:56:55','niraj','updatedBy','2014-07-28 07:33:01',2),(13,'dell','laptop4','latitude',0,'2014-07-21 19:58:06','niraj','updatedBy','2014-07-21 14:28:06',1),(14,'dell','laptop4','latitude',0,'2014-07-21 19:58:07','niraj','updatedBy','2014-07-21 14:28:07',1),(15,'modifiedName15','modified15','latitude',0,'2014-07-21 19:58:09','niraj','adminX15','2014-07-23 12:33:21',1),(16,'dell','laptop5','latitude',0,'2014-07-21 20:00:23','niraj','updatedBy','2014-07-23 08:38:13',2),(17,'dell','laptop6','latitude',0,'2014-07-21 21:51:51','niraj','updatedBy','2014-07-24 11:12:28',2),(18,'sony','mobile','xperia',0,'2014-07-22 00:08:37','niraj','updatedBy','2014-07-21 18:38:37',1),(19,'dell','laptop7','latitude',0,'2014-07-22 11:45:36','niraj','updatedBy','2014-07-22 06:15:36',1),(20,'dell','laptop8','latitude',0,'2014-07-22 12:01:38','niraj','nk','2014-07-25 05:01:21',1),(21,'htc','mobile','dkf.fkgkgrkfld',0,'2014-07-22 12:02:53','niraj',NULL,'2014-07-24 09:49:33',2),(22,'newname2222','newtype22','kkk',0,'2014-07-22 12:31:08','niraj','adminX22','2014-07-24 09:52:02',1),(23,'dell','laptop8','latitude',0,'2014-07-22 12:32:28','niraj','nk','2014-07-22 07:02:28',1),(24,'dell','laptop3','latitude',0,'2014-07-22 12:33:19','niraj','updatedBy','2014-07-22 07:03:19',1),(25,'dell','laptop3','latitude',0,'2014-07-22 12:33:19','niraj','updatedBy','2014-07-22 07:03:19',1),(26,'dell','laptop8','latitude',0,'2014-07-22 12:33:25','niraj','nk','2014-07-22 07:03:25',1),(27,'dell','laptop8','latitude',0,'2014-07-22 12:33:32','niraj','nk','2014-07-22 07:03:32',1),(28,'mm','mm','mm',0,'2014-07-22 12:34:16','niraj',NULL,'2014-07-22 07:04:16',1),(29,'jj','jj','jj',0,'2014-07-22 12:34:16','niraj',NULL,'2014-07-22 07:04:16',1),(30,'ll','ll','ll',0,'2014-07-22 12:35:14','niraj',NULL,'2014-07-22 07:05:14',1),(31,'lll','ll','ll',0,'2014-07-22 12:35:43','niraj',NULL,'2014-07-22 07:05:43',1),(32,'ll','ll','ll',0,'2014-07-22 12:36:24','niraj',NULL,'2014-07-22 07:06:24',1),(33,'kk','kk','kk',0,'2014-07-22 12:37:54','niraj',NULL,'2014-07-22 07:07:54',1),(34,'lll','ll','ll',0,'2014-07-22 12:38:20','niraj',NULL,'2014-07-22 07:08:20',1),(35,'kk','kk','kk',0,'2014-07-22 12:39:17','niraj',NULL,'2014-07-22 07:09:17',1),(36,'nn','nn','nn',0,'2014-07-22 12:54:43','niraj',NULL,'2014-07-22 07:24:43',1),(37,'ss','ss','ss',0,'2014-07-22 12:55:59','niraj',NULL,'2014-07-22 07:25:59',1),(38,'ww','ww','ww',0,'2014-07-22 12:56:18','niraj',NULL,'2014-07-22 07:26:18',1),(39,'dell','laptop8','latitude',0,'2014-07-22 13:04:07','niraj','nk44444','2014-07-22 07:34:07',1),(40,'dell','laptop8','latitude',0,'2014-07-22 13:04:08','niraj','nk44444','2014-07-22 07:34:08',1),(41,'dell','laptop8','latitude',0,'2014-07-22 13:04:08','niraj','nk44444','2014-07-22 07:34:08',1),(42,'dell','laptop8','latitude',0,'2014-07-22 13:04:08','niraj','nk44444','2014-07-22 07:34:08',1),(43,'dell','laptop8','latitude',0,'2014-07-22 14:02:18','niraj2','nk55555','2014-07-22 08:32:18',1),(44,'dell','laptop8','latitude',0,'2014-07-22 14:02:29','niraj2','nk55555','2014-07-22 08:32:29',1),(45,'dell','laptop8','latitude',0,'2014-07-22 14:02:34','niraj2','nk55555','2014-07-22 08:32:34',1),(46,'dell','laptop8','latitude',0,'2014-07-22 14:02:37','niraj2','nk55555','2014-07-22 08:32:37',1),(47,'dell','laptop8','latitude',0,'2014-07-22 14:02:37','niraj2','nk55555','2014-07-22 08:32:37',1),(48,'dell','laptop3','latitude',0,'2014-07-22 14:05:00','niraj','updatedBy','2014-07-22 08:35:00',1),(49,'dell','laptop3','latitude',0,'2014-07-22 14:05:00','niraj','updatedBy','2014-07-22 08:35:00',1),(50,'dell','laptop8','latitude',0,'2014-07-22 14:05:13','niraj2','nk55555','2014-07-22 08:35:13',1),(51,'dell','laptop8','latitude',0,'2014-07-22 14:05:24','niraj2','nk55555','2014-07-22 08:35:24',1),(52,'dell','laptop8','latitude',0,'2014-07-22 14:05:24','niraj2','nk55555','2014-07-22 08:35:24',1),(53,'dell','laptop8','latitude',0,'2014-07-22 14:05:31','niraj2','nk55555','2014-07-22 08:35:31',1),(54,'dell','laptop8','latitude',0,'2014-07-22 14:05:34','niraj2','nk55555','2014-07-22 08:35:34',1),(55,'dell','laptop8','latitude',0,'2014-07-22 14:05:40','niraj2','nk55555','2014-07-22 08:35:40',1),(56,'dell','laptop8','latitude',0,'2014-07-22 14:05:40','niraj2','nk55555','2014-07-22 08:35:40',1),(57,'dell','laptop8','latitude',0,'2014-07-22 14:05:45','niraj2','nk55555','2014-07-22 08:35:45',1),(58,'dell','laptop8','latitude',0,'2014-07-22 14:05:47','niraj2','nk55555','2014-07-22 08:35:47',1),(59,'dell','laptop8','latitude',0,'2014-07-22 14:05:47','niraj2','nk55555','2014-07-22 08:35:47',1),(60,'dell','laptop8','latitude',0,'2014-07-22 14:05:48','niraj2','nk55555','2014-07-22 08:35:48',1),(61,'dell','laptop8','latitude',0,'2014-07-22 14:05:54','niraj2','nk55555','2014-07-22 08:35:54',1),(62,'dell','laptop8','latitude',0,'2014-07-22 14:05:58','niraj2','nk55555','2014-07-22 08:35:58',1),(63,'dell','laptop8','latitude',0,'2014-07-22 14:05:58','niraj2','nk55555','2014-07-22 08:35:58',1),(64,'dell','laptop8','latitude',0,'2014-07-22 14:06:00','niraj2','nk55555','2014-07-22 08:36:00',1),(65,'dell','laptop8','latitude',0,'2014-07-22 14:06:00','niraj2','nk55555','2014-07-22 08:36:00',1),(66,'dell','laptop8','latitude',0,'2014-07-22 14:06:04','niraj2','nk55555','2014-07-22 08:36:04',1),(67,'dell','laptop8','latitude',0,'2014-07-22 14:06:24','niraj2','nk55555','2014-07-22 08:36:24',1),(68,'dell','laptop8','latitude',0,'2014-07-22 14:06:24','niraj2','nk55555','2014-07-22 08:36:24',1),(69,'dell','laptop8','latitude',0,'2014-07-22 14:12:22','niraj2','nk55555','2014-07-22 08:42:22',1),(70,'dell','laptop8','latitude',0,'2014-07-22 14:12:22','niraj2','nk55555','2014-07-22 08:42:22',1),(71,'dell','laptop8','latitude',0,'2014-07-22 14:12:22','niraj2','nk55555','2014-07-22 08:42:22',1),(72,'dell','laptop8','latitude',0,'2014-07-22 14:12:23','niraj2','nk55555','2014-07-22 08:42:23',1),(73,'dell','laptop8','latitude',0,'2014-07-22 14:12:23','niraj2','nk55555','2014-07-22 08:42:23',1),(74,'dell','laptop3','latitude',0,'2014-07-22 14:12:42','niraj','updatedBy','2014-07-22 08:42:42',1),(75,'dell','laptop3','latitude',0,'2014-07-22 14:12:42','niraj','updatedBy','2014-07-22 08:42:42',1),(76,'dell','laptop8','latitude',0,'2014-07-22 14:12:51','niraj2','nk55555','2014-07-22 08:42:51',1),(77,'dell','laptop8','latitude',0,'2014-07-22 14:12:52','niraj2','nk55555','2014-07-22 08:42:52',1),(78,'dell','laptop8','latitude',0,'2014-07-22 14:12:52','niraj2','nk55555','2014-07-22 08:42:52',1),(79,'dell','laptop8','latitude',0,'2014-07-22 14:12:59','niraj2','nk55555','2014-07-22 08:42:59',1),(80,'dell','laptop8','latitude',0,'2014-07-22 14:13:00','niraj2','nk55555','2014-07-22 08:43:00',1),(81,'dell','laptop8','latitude',0,'2014-07-22 14:13:00','niraj2','nk55555','2014-07-22 08:43:00',1),(82,'dell','laptop8','latitude',0,'2014-07-22 14:13:00','niraj2','nk55555','2014-07-22 08:43:00',1),(83,'dell','laptop8','latitude',0,'2014-07-22 14:13:00','niraj2','nk55555','2014-07-22 08:43:00',1),(84,'dell','laptop8','latitude',0,'2014-07-22 14:13:00','niraj2','nk55555','2014-07-22 08:43:00',1),(85,'dell','laptop8','latitude',0,'2014-07-22 14:13:01','niraj2','nk55555','2014-07-22 08:43:01',1),(86,'dell','laptop8','latitude',0,'2014-07-22 14:13:02','niraj2','nk55555','2014-07-22 08:43:02',1),(87,'dell','laptop8','latitude',0,'2014-07-22 14:13:02','niraj2','nk55555','2014-07-22 08:43:02',1),(88,'dell','laptop8','latitude',0,'2014-07-22 14:13:03','niraj2','nk55555','2014-07-22 08:43:03',1),(89,'dell','laptop8','latitude',0,'2014-07-22 14:13:03','niraj2','nk55555','2014-07-22 08:43:03',1),(90,'dell','laptop8','latitude',0,'2014-07-22 14:13:03','niraj2','nk55555','2014-07-22 08:43:03',1),(91,'dell','laptop8','latitude',0,'2014-07-22 14:13:12','niraj2','nk55555','2014-07-22 08:43:12',1),(92,'dell','laptop8','latitude',0,'2014-07-22 14:13:12','niraj2','nk55555','2014-07-22 08:43:12',1),(93,'dell','laptop','',0,'2014-07-22 14:36:22','niraj',NULL,'2014-07-22 09:06:22',1),(94,'acer','mouse','wireless',0,'2014-07-22 15:09:44','niraj','shikha','2014-07-22 09:39:44',1),(95,'acer','mouse','wireless',0,'2014-07-22 15:09:44','niraj','shikha','2014-07-22 09:39:44',1),(96,'acer','mouse','wireless',0,'2014-07-22 15:57:53','niraj','shikha','2014-07-22 10:27:53',1),(97,'dell','laptop3','latitude',0,'2014-07-22 15:59:42','niraj','updatedBy','2014-07-22 10:29:42',1),(98,'dell','laptop3','latitude',0,'2014-07-22 16:19:22','niraj','updatedBy','2014-07-22 10:49:22',1),(99,'dell','laptop3','latitude',0,'2014-07-22 16:19:22','niraj','updatedBy','2014-07-25 11:49:11',1),(100,'modifiedName100','modified100','latitude',0,'2014-07-22 16:25:53','niraj','adminX100','2014-07-25 07:13:29',1),(101,'dell','laptop3','latitude',0,'2014-07-22 16:25:53','niraj','updatedBy','2014-07-22 10:55:53',1),(102,'dell','laptop3','latitude',0,'2014-07-22 16:27:12','niraj','updatedBy','2014-07-25 10:58:05',2),(103,'dell','laptop3','latitude',0,'2014-07-22 16:27:12','niraj','updatedBy','2014-07-25 10:56:18',2),(104,'dell','laptop3','latitude',0,'2014-07-22 18:37:07','niraj','updatedBy','2014-07-25 09:47:27',2),(105,'testName','testType','testDescription',0,'2014-07-22 18:37:07','niraj','testAdmin','2014-07-25 11:54:37',1),(106,'modifiedName22','modifiedagain','new desc',0,'2014-07-23 10:25:17','niraj','me','2014-07-23 04:55:17',1),(107,'modifiedName22','modifiedagain','new desc',0,'2014-07-23 10:25:17','niraj','me','2014-07-23 04:55:17',1),(108,'kfjgri','lkgjrij','kjfirk',0,'2014-07-23 16:31:12','niraj',NULL,'2014-07-23 11:01:12',1),(109,'mine','abc','',0,'2014-07-24 16:33:58','niraj',NULL,'2014-07-24 11:03:58',1),(110,'samsung','keyboard','new',0,'2014-07-24 16:48:47','niraj',NULL,'2014-07-24 11:18:47',1),(111,'','','',0,'2014-07-24 16:49:08','niraj',NULL,'2014-07-24 11:19:08',1),(112,'','','',0,'2014-07-24 16:49:35','niraj',NULL,'2014-07-24 11:19:35',1),(113,'','%%%%','',0,'2014-07-24 16:49:57','niraj',NULL,'2014-07-24 11:58:06',3),(114,'','','',0,'2014-07-25 11:46:32','niraj',NULL,'2014-07-25 06:16:32',1),(115,'','',NULL,0,'2014-07-25 11:52:47','adminBlank',NULL,'2014-07-25 06:22:47',1),(116,'llllllllll','jjjjjjj','',0,'2014-07-25 12:09:28','niraj',NULL,'2014-07-25 06:39:28',1),(117,'mdnkffmk','kmlrmkfm','',0,'2014-07-25 12:38:44','niraj',NULL,'2014-07-25 07:08:44',1),(118,'dell','laptop','',0,'2014-07-25 15:05:10','niraj',NULL,'2014-07-25 09:35:10',1),(119,'acer','laptop','',0,'2014-07-25 15:55:02','niraj',NULL,'2014-07-25 10:25:02',1),(120,'acer','laptop','',0,'2014-07-25 15:55:04','niraj',NULL,'2014-07-25 10:25:04',1),(121,'acer','laptop','',0,'2014-07-25 15:55:15','niraj',NULL,'2014-07-25 10:25:15',1),(122,'dell','laptop','',0,'2014-07-25 16:04:46','niraj',NULL,'2014-07-25 10:34:46',1),(123,'jfojf','pokfposdfe','',0,'2014-07-25 16:06:42','niraj',NULL,'2014-07-25 10:36:42',1),(124,'jhkjkjkjkjlklk','hjhjjjjkkjkj','',0,'2014-07-25 16:09:37','niraj',NULL,'2014-07-25 10:39:37',1),(125,'djidvjsdklvl','jdvijkj','',0,'2014-07-25 16:13:31','niraj',NULL,'2014-07-25 10:43:31',1),(126,'djidvjsdklvl','jdvijkj','',0,'2014-07-25 16:13:31','niraj',NULL,'2014-07-25 10:43:31',1),(127,'djidvjsdklvl','jdvijkj','',0,'2014-07-25 16:13:31','niraj',NULL,'2014-07-25 10:43:31',1),(128,'jhkjkjkjkjlklk','hjhjjjjkkjkj','',0,'2014-07-25 16:13:39','niraj',NULL,'2014-07-25 10:43:39',1),(129,'jhkjkjkjkjlklk','hjhjjjjkkjkj','',0,'2014-07-25 16:13:40','niraj',NULL,'2014-07-25 10:43:40',1),(130,'hcsjkkdx','jhgjfghlj','',0,'2014-07-25 16:13:56','niraj',NULL,'2014-07-25 10:43:56',1),(131,'kvdl','mscl','',0,'2014-07-25 16:13:58','niraj',NULL,'2014-07-25 10:43:58',1),(132,'eryer','reyer','',0,'2014-07-25 16:25:04','niraj',NULL,'2014-07-25 10:55:04',1),(133,'kmdl','sjfoksz','',0,'2014-07-25 16:25:36','niraj',NULL,'2014-07-25 10:55:36',1),(134,'kmdl','sjfoksz','',0,'2014-07-25 16:26:08','niraj',NULL,'2014-07-25 10:56:08',1),(135,'mmmmm','mmmmmmm','',0,'2014-07-25 16:30:08','niraj',NULL,'2014-07-25 11:00:08',1),(136,'acer','laptop','',0,'2014-07-25 16:49:44','niraj',NULL,'2014-07-25 11:19:44',1),(137,'mmmm','mmmmmmmm','mm',0,'2014-07-25 16:53:18','niraj',NULL,'2014-07-25 11:23:18',1),(138,'aaaaaaaaaaa','mmmmmmmmmmm','',0,'2014-07-25 16:54:23','niraj',NULL,'2014-07-25 11:24:23',1),(139,'qqqqqqqqqqqqqq','llllllllllllll','',0,'2014-07-25 16:55:44','niraj',NULL,'2014-07-25 11:25:44',1),(140,'mmmmmmmmmmm','nnnnnnnnnnn','',0,'2014-07-25 16:56:27','niraj',NULL,'2014-07-25 11:26:27',1),(141,'addtest','test','ondemand',0,'2014-07-25 17:01:52','me',NULL,'2014-07-25 11:31:52',1),(142,'dchjdcdkjjkl','jhhjhkjkkk','jkdcvgvhgn',0,'2014-07-25 17:02:42','niraj',NULL,'2014-07-25 11:32:42',1),(143,'eeeeeeeeeeeeeeeeeee','eeeeeeeeeeee','',0,'2014-07-25 17:06:07','niraj',NULL,'2014-07-25 11:36:07',1),(144,',,,,,,,,,,,,,,,,,,,,',',,,,,,,,,,,,,,,,,,,,,,,,','',0,'2014-07-25 17:07:30','niraj',NULL,'2014-07-25 11:37:30',1),(145,'dvsdz','vzxv','',0,'2014-07-25 17:11:12','niraj',NULL,'2014-07-25 11:41:12',1),(146,'gd','rgrf','',0,'2014-07-25 17:13:19','niraj',NULL,'2014-07-25 11:43:19',1),(147,'dsvsd','dvsd','',0,'2014-07-25 17:13:52','niraj',NULL,'2014-07-25 11:43:52',1),(148,'ffffffff','ffffffffffff','',0,'2014-07-25 17:14:38','niraj',NULL,'2014-07-25 11:44:38',1),(149,'hlkhlk','hlkh','',0,'2014-07-25 17:16:23','niraj',NULL,'2014-07-25 11:46:23',1),(150,'kdhefjkflk','jkfhjhfjn','',0,'2014-07-25 17:19:20','niraj',NULL,'2014-07-25 11:49:20',1),(151,'qqqqqqqqq','qqqqqqqqqqq','',0,'2014-07-25 17:53:09','niraj',NULL,'2014-07-25 12:23:09',1),(152,'debug1','gjgj','ngjhgh',0,'2014-07-28 12:39:57','nk',NULL,'2014-07-28 07:09:57',1),(153,'modifiedNameTest','modifiedTest','ngjhgh',0,'2014-07-28 12:46:05','nk','BackEnd','2014-07-28 07:27:11',2);
/*!40000 ALTER TABLE `INVENTORY` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-28 14:20:39








CREATE DATABASE  IF NOT EXISTS `test_team3_invMngmt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_team3_invMngmt`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: test_team3_invMngmt
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TRANSACTION`
--

DROP TABLE IF EXISTS `TRANSACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TRANSACTION` (
  `transactionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemId` int(10) unsigned NOT NULL,
  `employeeId` varchar(254) NOT NULL,
  `namespaceId` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `issuedBy` varchar(254) NOT NULL,
  `issuedOn` datetime DEFAULT NULL,
  `expectedReturn` datetime DEFAULT NULL,
  `returnedOn` datetime DEFAULT NULL COMMENT '\n',
  `returnedTo` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`transactionId`),
  KEY `EMPLOYEE_ID` (`employeeId`),
  KEY `fk_TRANSACTION_1` (`status`),
  CONSTRAINT `fk_TRANSACTION_1` FOREIGN KEY (`status`) REFERENCES `TRANSACTION_STATUS` (`statusNumber`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TRANSACTION`
--

LOCK TABLES `TRANSACTION` WRITE;
/*!40000 ALTER TABLE `TRANSACTION` DISABLE KEYS */;
INSERT INTO `TRANSACTION` VALUES (6,9,'nkumar1034',0,2,'adminX','2014-07-23 11:17:51',NULL,'2014-07-23 11:18:10','adminR'),(7,15,'nkumar15',0,2,'adminX15','2014-07-23 13:34:00',NULL,'2014-07-23 15:55:14','adminR15'),(8,16,'nkumar16',0,1,'adminX16','2014-07-23 14:08:13','2014-12-12 00:00:00',NULL,NULL),(9,20,'nkumar20',0,2,'adminX20','2014-07-24 11:36:02',NULL,'2014-07-25 10:31:21','adminR20'),(10,17,'nkumar16',0,2,'adminX2','2014-07-24 16:38:48',NULL,'2014-07-24 16:41:59','adminR'),(11,17,'nkumar16',0,1,'adminX2again','2014-07-24 16:42:28',NULL,NULL,NULL),(12,104,'nkumar',0,1,'adminX','2014-07-25 15:17:27','2014-07-22 13:13:16',NULL,NULL),(13,105,'nkumar',0,2,'adminX','2014-07-25 15:44:53','2014-07-22 13:13:16','2014-07-25 17:24:37','testAdminR105'),(14,103,'nkumar',0,1,'adminX','2014-07-25 16:26:18','2014-07-22 13:13:16',NULL,NULL),(15,102,'nkumar',0,1,'adminX','2014-07-25 16:28:05','2014-07-22 13:13:01',NULL,NULL),(16,105,'test105',0,2,'testAdmin','2014-07-25 16:43:38','2014-08-15 10:15:00','2014-07-25 17:24:37','testAdminR105'),(17,99,'test99',0,2,'testAdmin','2014-07-25 16:46:24','2014-08-15 10:15:00','2014-07-25 16:53:50','adminR'),(18,99,'test99',0,2,'testAdmin','2014-07-25 16:54:46','2014-08-15 10:15:00','2014-07-25 16:55:53','adminR2'),(19,99,'test99',0,2,'testAdmin','2014-07-25 17:04:49','2014-08-15 10:15:00','2014-07-25 17:05:33','adminR2'),(20,99,'test99',0,2,'testAdmin','2014-07-25 17:05:46','2014-08-15 10:15:00','2014-07-25 17:19:11','adminR2'),(21,153,'all',0,2,'adminXBE','2014-07-28 12:52:45','2014-07-22 13:13:16','2014-07-28 12:55:21','adminRBE'),(22,153,'all',0,2,'adminXBE2','2014-07-28 12:56:11','2014-07-22 13:13:16','2014-07-28 12:56:41','adminRBE2'),(23,153,'all',0,1,'adminXBE3','2014-07-28 12:57:11','2014-07-22 13:13:16',NULL,NULL),(24,12,'all',0,1,'adminXBE12','2014-07-28 13:03:01','2014-07-22 13:13:16',NULL,NULL);
/*!40000 ALTER TABLE `TRANSACTION` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-28 14:20:39







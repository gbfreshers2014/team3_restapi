/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.inventoryManagement.restlet.InventoryManagementService;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<InventoryManagementService> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new InventoryManagementService());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(InventoryManagementService app) {
        super(app);
    }
}

/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet.response;

import java.util.List;

import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Transection List server Resource.
 * @author Ipsita
 */
public class TransectionResponse extends AbstractResponse {

    private List<Transections> trans;

    /**
     * @return List<Transections> object
     */
    public List<Transections> getTrans() {
        return trans;
    }

    /**
     * @param trans List<Transections>
     */
    public void setTrans(List<Transections> trans) {
        this.trans = trans;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InventoryListResponse [Transections =" + this.trans + "]";
    }

}

/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet.response;

import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Inventory server Response.
 * @author Ipsita
 */
public class InventoryResponse extends AbstractResponse {

    private Inventory inventory;
    private ResponseStatus status;

    /**
     * @param inventory Inventory
     * @param status ResponseStatus
     */
    public InventoryResponse(Inventory inventory, ResponseStatus status) {
        this.inventory = inventory;
        this.status = status;
    }

    /**
     * default constructor.
     */
    public InventoryResponse() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @return Inventory object
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * @param inventory Inventory
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * @return ResponseStatus object
     */
    public ResponseStatus getStatus() {
        return status;
    }

    /**
     * @param status ResponseStatus
     */
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

}

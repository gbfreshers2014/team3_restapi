/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.restlet.resources;

/**
 * @author Ipsita
 */
public final class ResourcePath {

    public static final String URL_ADD_AND_GET_INVENTORY_LIST_RESOURCE = "/inventoryItems.json";
    public static final String URL_MODIFY_INVENTORY_RESOURCE = "/inventoryItems/{itemId}.json";
    public static final String URL_EMPLOYEE_TRANSACTION_INVENTORY_RESOURCE = "/inventory/transactions/{issuedTo}.json";

    /**
     * Default constructor.
     */
    private ResourcePath() {

    }
}

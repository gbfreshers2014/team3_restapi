/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet.resources;

import java.util.List;

import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.AddGetInventoryEntityManager;
import com.gwynniebee.inventoryManagement.Entities.ModifyInventoryEntityManager;
import com.gwynniebee.inventoryManagement.Entities.TransectionEntityManager;
import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author Niraj
 */
public class ModifyInventoryResource extends AbstractServerResource {

    private int itemId;
    private static final Logger LOG = LoggerFactory.getLogger(AddGetInventoryResource.class);
    private static InventoryResponse response = new InventoryResponse();
    private ResponseStatus rstatus = new ResponseStatus(0, "success");

    // private Transections trans = new Transections();

    @Override
    protected void doInit() {
        super.doInit();

        try {
            String s = (String) this.getRequestAttributes().get("itemId");
            this.itemId = Integer.parseInt(s);
        } catch (Exception e) {
            return;
        }
    }

    /**
     * process.
     * @return ResponseStatus
     */
    @Delete
    public ResponseStatus deleteInventory() {
        int reply;
        try {
            reply = ModifyInventoryEntityManager.getInstance().deleteItem(this.itemId);
        } catch (Exception e) {
            return new ResponseStatus(ResponseStatus.SERVER_ERROR, "Delete failed" + e);
        }
        if (reply == -1) {
            return new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ, "item is issued or already deleted");
        }

        return new ResponseStatus(ResponseStatus.CODE_SUCCESS, "success");
    }

    /**
     * process.
     * @return InventoryResponse object
     */
    @Get
    public InventoryResponse getInventory() {
        List<Inventory> inventorys = null;
        FilterParams filterParams = new FilterParams();
        filterParams.addCriteria("itemId", Integer.toString(this.itemId));
        filterParams.setTable("INVENTORY");
        inventorys = AddGetInventoryEntityManager.getInstance().getInventory(filterParams);
        /************* ADDED CODE *******************/
        LOG.info("going to get data from transaction....");

        // TODO status =2 and itemId it will return a list
        // ===============================================
        // List<Inventory> finalinventorys = new
        // ArrayList<Inventory>(inventorys);

        for (int i = 0; i < inventorys.size(); i++) { // here size will always
                                                      // be 1
            List<Transections> transinves = null;
            transinves = TransectionEntityManager.getInstance().getTransaction(inventorys.get(i).getItemId(), 2);
            if (transinves.size() > 0) {
                Transections tran = transinves.get(transinves.size() - 1);
                inventorys.get(i).setLastIssuedTo(tran.getEmployeeId());
                inventorys.get(i).setReturnedOn(tran.getReturnedOn());
            }

            /************ added code for issued item *********/
            transinves = TransectionEntityManager.getInstance().getTransaction(inventorys.get(i).getItemId(), 1);
            if (transinves.size() > 0) {
                Transections tran = transinves.get(0);
                inventorys.get(i).setIssuedTo(tran.getEmployeeId());
                inventorys.get(i).setIssuedOn(tran.getIssuedOn());
                inventorys.get(i).setIssuedBy(tran.getIssuedBy());
                inventorys.get(i).setExpectedReturn(tran.getExpectedReturn());
            }
        }
        response.setStatus(new ResponseStatus(ResponseStatus.CODE_SUCCESS, "success"));
        response.setInventory(inventorys.get(0));
        return response;
    }

    /********************************************************/
    /**
     * process.
     * @param inventory Inventory
     * @return InventoryResponse object
     */
    @Put("application/json")
    public InventoryResponse modifyInventory(Inventory inventory) {
        LOG.info("modifyInventory :", inventory.toString());
        response.setStatus(rstatus);
        // String j = inventory.getJob();
        inventory.setItemId(this.itemId);
        String job = checkJob(inventory);
        if (job.equals("modify")) {
            return modify(inventory);
        } else if (job.equals("assign")) {
            return assign(inventory);
        } else if (job.equals("return")) {
            return returnItem(inventory);
        }
        // rstatus = new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ,
        // "undefined job");
        // response.setStatus(rstatus);
        return response;
    }

    /**
     * process.
     * @param inventory Inventory
     * @return InventoryResponse object
     */
    private InventoryResponse returnItem(Inventory inventory) {
        // TODO Auto-generated method stub
        // get transactionId by itemId and status
        // update status
        // update returnedTo, returnedOn
        // return inventory deatils
        try {
            response.setInventory(ModifyInventoryEntityManager.getInstance().returnInventory(inventory)); // why
                                                                                                          // warning?
            // putTransDetails(response,TransDetailsEntityManager.getInstance().getTransDetails(inventory.getItemId()));

        } catch (Exception e) {

            response.setStatus(new ResponseStatus(ResponseStatus.SERVER_ERROR, "return failed" + e));
        }
        return response;
    }

    /**
     * process.
     * @param inventory Inventory
     * @return InventoryResponse object
     */
    private InventoryResponse assign(Inventory inventory) {
        // check status is available
        // make it unavailable
        // create entry in TRANSACTION table with all the fields
        // return
        try {
            response.setInventory(ModifyInventoryEntityManager.getInstance().assignInventory(inventory)); // why
                                                                                                          // warning?
            // putTransDetails(response,TransDetailsEntityManager.getInstance().getTransDetails(inventory.getItemId()));

        } catch (Exception e) {

            response.setStatus(new ResponseStatus(ResponseStatus.SERVER_ERROR, "assignment failed" + e));
        }
        return response;
    }

    /**
     * process.
     * @param inventory Inventory
     * @return InventoryResponse object
     */
    private InventoryResponse modify(Inventory inventory) {
        // TODO Auto-generated method stub
        try {
            response.setInventory(ModifyInventoryEntityManager.getInstance().modifyInventory(inventory)); // why
                                                                                                          // warning?
            // putTransDetails(response,TransDetailsEntityManager.getInstance().getTransDetails(inventory.getItemId()));

        } catch (Exception e) {

            response.setStatus(new ResponseStatus(ResponseStatus.SERVER_ERROR, "assignment failed" + e));
        }
        return response;
    }

    /*
     * private void putTransDetails(InventoryResponse r, Transections t) { //
     * TODO Auto-generated method stub
     * r.getInventory().setIssuedTo(t.getEmployeeId()); //
     * r.getInventory().setIssuedOn(t.getIssuedOn()); }
     */
    /**
     * process.
     * @param inventory Inventory
     * @return job type or error message
     */
    private String checkJob(Inventory inventory) {
        Inventory oldInventory;
        AddGetInventoryDAO dao = null;
        try {
            oldInventory = AddGetInventoryEntityManager.getInstance().getInventoryById(inventory.getItemId());
        } finally {
            if (dao != null) {
                dao.close();
            }
        }

        /***** assign request ******/
        if (inventory.getJob().equals("assign")) { // check if
                                                   // name,type,description are
                                                   // same
            if (oldInventory.getStatus() == 1 && comp(oldInventory.getItemName(), inventory.getItemName())
                    && comp(oldInventory.getItemType(), inventory.getItemType())
                    && comp(oldInventory.getDescription(), inventory.getDescription())) {
                return "assign";
            } else {
                rstatus =
                        new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ,
                                "assign request attempting to change item details or item not available");
                response.setStatus(rstatus);
                return "invalidRequest";
            }
        }

        /***** return request ******/
        if (inventory.getJob().equals("return")) {
            if (oldInventory.getStatus() == 2 && comp(oldInventory.getItemName(), inventory.getItemName())
                    && comp(oldInventory.getItemType(), inventory.getItemType())
                    && comp(oldInventory.getDescription(), inventory.getDescription())) {
                return "return";
            } else {
                rstatus =
                        new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ,
                                "return request attempting to change item detail or item not issued");
                response.setStatus(rstatus);
                return "invalidRequest";
            }
        }

        /***** modify request ******/
        if (inventory.getJob().equals("modify")) {
            if (inventory.getItemName() == null) {
                inventory.setItemName(oldInventory.getItemName());
            }
            if (inventory.getItemType() == null) {
                inventory.setItemType(oldInventory.getItemType());
            }
            if (inventory.getDescription() == null) {
                inventory.setDescription(oldInventory.getDescription());
            }
            return "modify";
        }

        rstatus = new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ, "invalid job");
        response.setStatus(rstatus);
        return "invalid request";
    }

    /**
     * @param s1
     * @param s2
     * @return true or false
     */
    private boolean comp(String s1, String s2) {
        if (s1 == null && s2 == null) {
            return true;
        }
        if (s2 == null) {
            return true;
        }
        return s1.equals(s2);
    }

}

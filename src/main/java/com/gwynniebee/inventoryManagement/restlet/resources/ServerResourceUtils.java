/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.inventoryManagement.restlet.resources;

import org.restlet.data.Form;

import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * General utility for server resource.
 * @author ipsita
 */
public final class ServerResourceUtils {
    /**
     * Default constructor.
     */
    private ServerResourceUtils() {
    }
    /**
     * Process the request and prepare the FilterParams.
     * @param serverResource serverResource.
     * @return FilterParams object
     */
    public static FilterParams prepareGetParams(AbstractServerResource serverResource) {

        FilterParams filterParams = new FilterParams();

        String itemName = getParamValue(serverResource, "itemName");
        if ((itemName != null) && (itemName.length() > 0)) {
            filterParams.addCriteria("itemName", itemName);
        }

        String status = getParamValue(serverResource, "status");
        if ((status != null) && (status.length() > 0)) {
            filterParams.addCriteria("status", status);
        }

        String itemType = getParamValue(serverResource, "itemType");
        if ((itemType != null) && (itemType.length() > 0)) {
            filterParams.addCriteria("itemType", itemType);
        }
        String itemId = getParamValue(serverResource, "itemId");
        if ((itemId != null) && (itemId.length() > 0)) {
            filterParams.addCriteria("itemId", itemId);
        }

        String limit = getParamValue(serverResource, "limit");
        if (limit != null && limit.length() > 0) {
            filterParams.addCriteria("limit", limit);
        }
        String offset = getParamValue(serverResource, "offset");
        if (offset != null && offset.length() > 0) {
            filterParams.addCriteria("offset", offset);
        }
        return filterParams;

    }

    private static String getParamValue(AbstractServerResource serverResource, String key) {
        Form queryParams = serverResource.getRequest().getResourceRef().getQueryAsForm();
        String value = queryParams.getFirstValue(key);
        return value;
    }

}

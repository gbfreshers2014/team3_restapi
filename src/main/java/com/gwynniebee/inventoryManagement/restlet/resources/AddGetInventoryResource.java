/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet.resources;

import java.util.ArrayList;
import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.AddGetInventoryEntityManager;
import com.gwynniebee.inventoryManagement.Entities.TransectionEntityManager;
import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryListResponse;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Add Inventory server Resource.
 * @author Ipsita
 */
public class AddGetInventoryResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(AddGetInventoryResource.class);

    /**
     * process.
     * @param inventory Inventory
     * @return InventoryResponse object
     */
    @Post("application/json")
    public InventoryResponse addInventory(Inventory inventory) {
        LOG.info("Request : " + inventory.toString());
        InventoryResponse response = new InventoryResponse();

        ResponseStatus rstatus;
        rstatus = new ResponseStatus(ResponseStatus.CODE_SUCCESS, "success");
        response.setStatus(rstatus);

        if (inventory == null || inventory.getItemName() == null || inventory.getItemType() == null
                || inventory.getItemName().length() <= 0 || inventory.getItemType().length() <= 0) {
            response.setStatus(new ResponseStatus(ResponseStatus.ERR_CODE_INVALID_REQ, "itemName or itemType cannot be null"));
            return response;
        }

        try {
            response.setInventory(AddGetInventoryEntityManager.getInstance().addItem(inventory));
        } catch (Exception e) {
            response.getStatus().setMessage(e.toString());
        }
        return response;
    }

    /**
     * process.
     * @return InventoryListResponse
     */
    @Get
    public InventoryListResponse getInventory() {

        LOG.info("Request to get all the inventory");
        InventoryListResponse response = new InventoryListResponse();
        List<Inventory> inventorys = null;
        FilterParams filterParams = ServerResourceUtils.prepareGetParams(this);
        if (filterParams.getCriteria().get("limit") != null) {
            int limit = Integer.parseInt(filterParams.getCriteria().get("limit").toString());
            if (limit <= 0) {
                getRespStatus().setMessage("Limit Should be a Positive Number");
                setStatus(Status.SUCCESS_OK);
                response.setStatus(getRespStatus());
                return response;
            }
        }

        filterParams.setTable("INVENTORY");

        inventorys = AddGetInventoryEntityManager.getInstance().getInventory(filterParams);

        /************* ADDED CODE *******************/
        LOG.info("going to get data from transaction");

        // TODO status =2 and itemId it will return a list
        // ===============================================
        List<Inventory> finalinventorys = new ArrayList<Inventory>(inventorys);

        for (int i = 0; i < inventorys.size(); i++) {
            List<Transections> transinves = null;
            transinves = TransectionEntityManager.getInstance().getTransaction(inventorys.get(i).getItemId(), 2);
            if (transinves.size() > 0) {
                Transections tran = transinves.get(transinves.size() - 1);
                finalinventorys.get(i).setLastIssuedTo(tran.getEmployeeId());
                finalinventorys.get(i).setReturnedOn(tran.getReturnedOn());
            }

            /************ added code for issued item *********/
            transinves = TransectionEntityManager.getInstance().getTransaction(inventorys.get(i).getItemId(), 1);
            if (transinves.size() > 0) {
                Transections tran = transinves.get(0);
                finalinventorys.get(i).setIssuedTo(tran.getEmployeeId());
                finalinventorys.get(i).setIssuedOn(tran.getIssuedOn());
                finalinventorys.get(i).setIssuedBy(tran.getIssuedBy());
                finalinventorys.get(i).setExpectedReturn(tran.getExpectedReturn());
            }
        }
        /********** Added code done ****/

        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("success");
        setStatus(Status.SUCCESS_OK);
        response.setInventorys(finalinventorys);
        response.setStatus(getRespStatus());

        return response;
    }

}

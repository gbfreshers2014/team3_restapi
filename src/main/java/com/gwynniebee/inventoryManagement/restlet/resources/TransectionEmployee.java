/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.restlet.resources;

import java.util.ArrayList;
import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.Entities.AddGetInventoryEntityManager;
import com.gwynniebee.inventoryManagement.Entities.TransectionEntityManager;
import com.gwynniebee.inventoryManagement.object.Inventory;
import com.gwynniebee.inventoryManagement.object.Transections;
import com.gwynniebee.inventoryManagement.restlet.response.InventoryListResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Resource file for Transection.
 * @author Ipsita
 */
public class TransectionEmployee extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(TransectionEmployee.class);

    /**
     * Return the list of all issued inventories to a given employeeId.
     * @return InventoryListResponse object
     */
    @Get
    public InventoryListResponse getInventoryByEmployeeID() {

        LOG.info("Request to get inventory details from Tansectiona and Inventory ");
        String employeeId = (String) getRequest().getAttributes().get("issuedTo");
        InventoryListResponse tranresponse = new InventoryListResponse();
        List<Transections> traninventory;

        traninventory = TransectionEntityManager.getInstance().getTransactionbyEmployeeID(employeeId);

        List<Inventory> emloyeeIssuedInventory = new ArrayList<Inventory>(traninventory.size());
        Transections trans = null;
        for (int i = 0; i < traninventory.size(); i++) {
            Inventory inv = new Inventory();
            trans = traninventory.get(i);
            inv = AddGetInventoryEntityManager.getInstance().getInventoryById(trans.getItemId());
            inv.setIssuedTo(employeeId);
            inv.setIssuedOn(trans.getIssuedOn());
            inv.setExpectedReturn(trans.getExpectedReturn());
            inv.setIssuedBy(trans.getIssuedBy());
            emloyeeIssuedInventory.add(inv);
            trans = null;
        }

        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("success in getting the details from Transection and invetory tables");
        setStatus(Status.SUCCESS_OK);

        tranresponse.setInventorys(emloyeeIssuedInventory);

        tranresponse.setStatus(getRespStatus());

        return tranresponse;
    }
}

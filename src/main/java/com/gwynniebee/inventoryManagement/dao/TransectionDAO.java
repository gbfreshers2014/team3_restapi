/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryManagement.dao.mapper.TransDetailsMapper;
import com.gwynniebee.inventoryManagement.object.Transections;
/**
 * Mapper class for Transetions.
 * @author Ipsita
 *
 */
@RegisterMapper(TransDetailsMapper.class)
public interface TransectionDAO extends Transactional<TransectionDAO> {

    /**
     * Modify tranaction into database.
     * @param itemId {@link transection}
     * @param status {@link transection}
     * @return list of Transctions details which are having same itemId and
     *         status
     */
    @SqlQuery("select  * from TRANSACTION where itemId = :itemId AND status=:status")
    List<Transections> getLastIssuedRETURNEDOn(@Bind("itemId") int itemId, @Bind("status") int status);

    /**
     * Modify tranaction into database.
     * @param employeeId {@link transection}
     * @return list of Transctions which are having same employeeId
     */
    @SqlQuery("select  * from TRANSACTION where employeeId = :employeeId AND status=1")
    List<Transections> getTransectionByemployeeId(@Bind("employeeId") String employeeId);

    /**
     * closes the underlying connection.
     */
    void close();
}

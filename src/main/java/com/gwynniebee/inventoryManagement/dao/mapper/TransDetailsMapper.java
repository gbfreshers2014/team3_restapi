/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.dao.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryManagement.object.Transections;

/**
 * @author Ipsita
 */
public class TransDetailsMapper implements ResultSetMapper<Transections> {

    @Override
    public Transections map(int index, ResultSet arg, StatementContext ctx) throws SQLException {
        // TODO Auto-generated method stub
        Transections inventory = new Transections();
        ResultSetMetaData metadata = arg.getMetaData();

        for (int i = 1; i <= metadata.getColumnCount(); ++i) {

            String name = metadata.getColumnLabel(i);

            if (name.equalsIgnoreCase("transactionId")) {
                inventory.setTransactionId(arg.getInt("transactionId"));
            } else if (name.equalsIgnoreCase("itemid")) {
                inventory.setItemId(arg.getInt("itemId"));
            } else if (name.equalsIgnoreCase("employeeId")) {
                inventory.setEmployeeId(arg.getString("employeeId"));
            } else if (name.equalsIgnoreCase("namespaceId")) {
                inventory.setNamespaceId(arg.getInt("namespaceId"));
            } else if (name.equalsIgnoreCase("status")) {
                inventory.setStatus(arg.getInt("status"));
            } else if (name.equalsIgnoreCase("issuedBy")) {
                inventory.setIssuedBy(arg.getString("issuedBy"));
            } else if (name.equalsIgnoreCase("issuedOn")) {
                Timestamp timestamp = arg.getTimestamp("issuedOn");
                if (timestamp != null) {
                    inventory.setIssuedOn(new DateTime(timestamp.getTime(), DateTimeZone.UTC));
                }
            } else if (name.equalsIgnoreCase("expectedReturn")) {
                Timestamp timestamp = arg.getTimestamp("expectedReturn");
                if (timestamp != null) {
                    inventory.setExpectedReturn(new DateTime(timestamp.getTime(), DateTimeZone.UTC));
                }
            } else if (name.equalsIgnoreCase("returnedOn")) {
                Timestamp timestamp = arg.getTimestamp("returnedOn");
                if (timestamp != null) {
                    inventory.setReturnedOn(new DateTime(timestamp.getTime(), DateTimeZone.UTC));
                }
            } else if (name.equalsIgnoreCase("returnedTo")) {
                inventory.setReturnTo(arg.getString("returnedTo"));
            }
        }

        return inventory;

    }

}

/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryManagement.object.Inventory;

/**
 * @author Niraj
 */
public class AddGetInventoryMapper implements ResultSetMapper<Inventory> {

    // private String[] status = {"Available","Not Available"};
    @Override
    public Inventory map(int arg0, ResultSet r, StatementContext arg2) throws SQLException {
        // TODO Auto-generated method stub

        Inventory inventory = new Inventory();

        inventory.setItemId(r.getInt("itemId"));

        inventory.setItemType(r.getString("itemType"));

        inventory.setItemName(r.getString("itemName"));

        inventory.setCreatedBy(r.getString("createdBy"));

        Timestamp t = r.getTimestamp("createdOn");
        DateTime d = new DateTime(t.getTime(), DateTimeZone.UTC);
        inventory.setCreatedOn(d);

        inventory.setUpdatedBy(r.getString("updatedBy"));
        inventory.setUpdatedOn(new DateTime(r.getTimestamp("updatedOn").getTime(), DateTimeZone.UTC));

        inventory.setDescription(r.getString("description"));

        inventory.setStatus(r.getInt("status"));

        return inventory;
    }

}

/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryManagement.dao.mapper.AddGetInventoryMapper;
import com.gwynniebee.inventoryManagement.object.Inventory;

/**
 * Database access dao for inventory.
 * @author Ipsita
 */
@RegisterMapper(AddGetInventoryMapper.class)
public interface AddGetInventoryDAO extends Transactional<AddGetInventoryDAO> {

    /**
     * Add inventory into database.
     * @param inventory {@link inventory} object to add
     * @return return auto generated id of inventory.
     */
    @SqlUpdate("insert into INVENTORY (itemName, itemType, description, createdOn, createdBy, "
            + " updatedOn, updatedBy, status) values (:i.itemName, :i.itemType,"
            + " :i.description, NOW(), :i.createdBy, NOW(), :i.updatedBy, 1)")
    @GetGeneratedKeys
    int addInventory(@BindBean("i") Inventory inventory);

    /**
     * Get inventory Details of given ItemId from database.
     * @param id {@link inventory} object to get
     * @return return all details item
     */
    @SqlQuery("select * from INVENTORY where itemId = :id")
    Inventory getInventoryById(@Bind("id") int id);

    /**
     * closes the underlying connection.
     */
    void close();
}

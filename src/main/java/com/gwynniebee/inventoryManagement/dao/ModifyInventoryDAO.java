/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryManagement.object.Inventory;

/**
 * Database access dao for inventory.
 * @author Niraj
 */

// @RegisterMapper(ModifyInventoryMapper.class) //not required
public interface ModifyInventoryDAO extends Transactional<ModifyInventoryDAO> {

    /**
     * Modify inventory into database.
     * @param inventory {@link inventoryItem} object to modify set the status =
     *            2 ( Not available)
     */
    @SqlUpdate("update INVENTORY set status=2 where itemId=:i.itemId")
    void assignInventory(@BindBean("i") Inventory inventory);

    /**
     * Modify inventory into database.
     * @param itemId {@link inventory} object to modify set the status = 3
     *            (deleted)
     */
    @SqlUpdate("update INVENTORY set status=3 where itemId=:itemId")
    void deleteInventory(@Bind("itemId") int itemId);

    /**
     * Modify inventory into database.
     * @param inventory {@link inventoryItem} object to modify
     */
    @SqlUpdate("update INVENTORY set itemName=:i.itemName, itemType=:i.itemType, "
            + "description=:i.description, updatedBy=:i.updatedBy where itemId=:i.itemId")
    void modifyInventory(@BindBean("i") Inventory inventory);

    /**
     * Modify inventory into database.
     * @param inventory {@link inventoryItem} object to modify set the staus = 1
     *            (Available)
     */
    @SqlUpdate("update INVENTORY set status=1 where itemId=:i.itemId")
    void returnInventory(@BindBean("i") Inventory inventory);

    /**
     * Modify inventory into database.
     * @param inventory {@link inventoryItem} object to modify update the fields
     *            status, returnOn and returnedTo
     */
    @SqlUpdate("update TRANSACTION set status=2, returnedTo=:i.returnedTo, returnedOn=NOW() " + "where itemId=:i.itemId and status=1")
    void updateTransReturn(@BindBean("i") Inventory inventory);

    /**
     * Modify transection into database.
     * @param inventory {@link inventory} object to modify
     */
    @SqlUpdate("insert into TRANSACTION (employeeId,itemId,issuedBy,status,issuedOn,expectedReturn) "
            + "values (:i.issuedTo,:i.itemId,:i.issuedBy,1,NOW(),:i.expectedReturnString)")
    void updateTransAssign(@BindBean("i") Inventory inventory);

    /**
     * closes the underlying connection.
     */
    void close();

}

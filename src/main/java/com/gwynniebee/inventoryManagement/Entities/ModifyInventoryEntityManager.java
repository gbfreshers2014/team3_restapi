/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.Entities;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.dao.ModifyInventoryDAO;
import com.gwynniebee.inventoryManagement.dao.mapper.ModifyInventoryMapper;
import com.gwynniebee.inventoryManagement.object.Inventory;

/**
 * Modify Entity Manager.
 * @author Niraj
 */
@RegisterMapper(ModifyInventoryMapper.class)
public final class ModifyInventoryEntityManager extends BaseEntityManager {

    private static final ModifyInventoryEntityManager INSTANCE = new ModifyInventoryEntityManager();

    /**
     * Return the instance for AddInventoryEntityManager.
     * @return instance
     */
    public static ModifyInventoryEntityManager getInstance() {
        return INSTANCE;
    }

    /**
     * default constructor.
     */
    private ModifyInventoryEntityManager() {

    }

    /**
     * @param inventory Inventory
     * @return Inventory object
     */
    public Inventory modifyInventory(Inventory inventory) {

        ModifyInventoryDAO dao = null;
        try {
            dao = getDbi().open(ModifyInventoryDAO.class);
            dao.modifyInventory(inventory);
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        // int i = inventory.getItemId();
        AddGetInventoryDAO dao2 = null;
        try {
            dao2 = getDbi().open(AddGetInventoryDAO.class);
            Inventory inventory2 = dao2.getInventoryById(inventory.getItemId());
            // String x = inventory2.getUpdatedBy();
            return inventory2;
        } finally {
            if (dao2 != null) {
                dao2.close();
            }
        }

    }

    /**
     * @param inventory Inventory
     * @return Inventory object
     */
    public Inventory returnInventory(Inventory inventory) {

        ModifyInventoryDAO dao = null;
        try {
            dao = getDbi().open(ModifyInventoryDAO.class);
            dao.returnInventory(inventory);
            dao.updateTransReturn(inventory);
            // call : getInventory(inventory.getItemId()) from AddInventory
            // class
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        // int i = inventory.getItemId();
        AddGetInventoryDAO dao2 = null;
        try {
            dao2 = getDbi().open(AddGetInventoryDAO.class);
            Inventory inventory2 = dao2.getInventoryById(inventory.getItemId());
            // String x = inventory2.getUpdatedBy();
            return inventory2;
        } finally {
            if (dao2 != null) {
                dao2.close();
            }
        }
    }

    /**
     * Assigns Inventory. Throws exception if catches one. Basically for
     * expectedReturn JSON-mysql format mismatch
     * @param inventory Details of inventory to be added, itemId, createdBy,
     *            issuedTo and expectedReturn.
     * @return inventory Details of added inventory
     * @throws Exception exception if sql-query fails, usually due to invalid dateTime format for expectedReturn
     */
    public Inventory assignInventory(Inventory inventory) throws Exception {
        ModifyInventoryDAO dao = null;
        try {
            dao = getDbi().open(ModifyInventoryDAO.class);
            dao.assignInventory(inventory);
            inventory.setExpectedReturnString(inventory.getExpectedReturn());
            dao.updateTransAssign(inventory);
        } catch (Exception e) {
            dao.returnInventory(inventory);
            throw e;
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        // int i = inventory.getItemId();
        AddGetInventoryDAO dao2 = null;
        try {
            dao2 = getDbi().open(AddGetInventoryDAO.class);
            Inventory inventory2 = dao2.getInventoryById(inventory.getItemId());
            // String x = inventory2.getUpdatedBy();
            return inventory2;
        } finally {
            if (dao2 != null) {
                dao2.close();
            }
        }
    }

    /**
     * @param itemId int
     * @return int
     */
    public int deleteItem(int itemId) {
        ModifyInventoryDAO dao = null;
        Inventory old;

        AddGetInventoryDAO dao2 = null;
        try {
            dao2 = getDbi().open(AddGetInventoryDAO.class);
            old = dao2.getInventoryById(itemId);
        } finally {
            if (dao2 != null) {
                dao2.close();
            }
        }

        if (old.getStatus() != 1) { // item is not available : it is issued or
                                    // already deleted
            return -1;
        }

        try {
            dao = getDbi().open(ModifyInventoryDAO.class);
            dao.deleteInventory(itemId);
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        return 1;
    }

}

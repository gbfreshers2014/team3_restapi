/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.Entities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.dao.TransectionDAO;
import com.gwynniebee.inventoryManagement.object.Transections;

/**
 * Transection Entity Manager.
 * @author Ipsita
 */

public class TransectionEntityManager extends BaseEntityManager {

    private static final TransectionEntityManager INSTANCE = new TransectionEntityManager();
    private static final Logger LOG = LoggerFactory.getLogger(AddGetInventoryEntityManager.class);

    /**
     * Default constructor.
     */
    public TransectionEntityManager() {
    }

    /**
     * Return the instance for InventoryEntityManager.
     * @return instance
     */
    public static TransectionEntityManager getInstance() {
        return INSTANCE;
    }

    /**
     * Return the list of Transactions matched with filters.
     * @param itemId int
     * @param status int
     * @return List<Transections>
     */
    public List<Transections> getTransaction(int itemId, int status) {
        LOG.info("inside Trasaction for geting last ReturnOn");
        TransectionDAO dao = null;
        List<Transections> transactioninventory;
        try {
            dao = getDbi().open(TransectionDAO.class);
            transactioninventory = dao.getLastIssuedRETURNEDOn(itemId, status);

            LOG.debug("geting the data" + transactioninventory.toString());

        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        return transactioninventory;
    }

    /*********** ADDED for transEmployee *************/
    /**
     * process.
     * @param employeeId String
     * @return List<Transections> object
     */
    public List<Transections> getTransactionbyEmployeeID(String employeeId) {
        LOG.info("inside Trasaction for geting last ReturnOn");
        TransectionDAO dao = null;
        List<Transections> transactioninventory;
        try {
            dao = getDbi().open(TransectionDAO.class);
            transactioninventory = dao.getTransectionByemployeeId(employeeId);

            LOG.debug("geting the data" + transactioninventory.toString());

        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        return transactioninventory;
    }

}

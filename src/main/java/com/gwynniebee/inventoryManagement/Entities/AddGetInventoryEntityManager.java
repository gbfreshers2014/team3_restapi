/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.Entities;

import java.util.List;

import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryManagement.dao.AddGetInventoryDAO;
import com.gwynniebee.inventoryManagement.dao.mapper.AddGetInventoryMapper;
import com.gwynniebee.inventoryManagement.object.FilterParams;
import com.gwynniebee.inventoryManagement.object.Inventory;

/**
 * Add and Get Entity Manager.
 * @author Ipsita
 */
@RegisterMapper(AddGetInventoryMapper.class)
public final class AddGetInventoryEntityManager extends BaseEntityManager {

    private static final AddGetInventoryEntityManager INSTANCE = new AddGetInventoryEntityManager();
    private static final Logger LOG = LoggerFactory.getLogger(AddGetInventoryEntityManager.class);

    /**
     * Default constructor.
     */
    private AddGetInventoryEntityManager() {
    }

    /**
     * Return the instance for AddInventoryEntityManager.
     * @return instance
     */
    public static AddGetInventoryEntityManager getInstance() {
        return INSTANCE;
    }

    /**
     * Add inventory into database.
     * @param inventory {@link inventory} object to add
     * @return return updated inventory after adding to database
     */
    public Inventory addItem(Inventory inventory) {
        // TODO Auto-generated method stub
        LOG.info("add Item request " + inventory.toString());
        AddGetInventoryDAO dao = null;
        try {
            dao = getDbi().open(AddGetInventoryDAO.class);
            int itemId = dao.addInventory(inventory);
            return getInventoryById(itemId);
        } finally {
            if (dao != null) {
                dao.close();
            }
        }

    }

    /**
     * Get inventory from database.
     * @param itemId {@link inventory} object to get
     * @return return inventory
     */
    public Inventory getInventoryById(int itemId) {
        AddGetInventoryDAO dao = null;
        try {
            dao = getDbi().open(AddGetInventoryDAO.class);

            Inventory inventory = dao.getInventoryById(itemId);
            return inventory;
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
    }

    /**
     * Get inventory from database.
     * @param filterParams {@link inventory} object to get
     * @return return inventory List
     */
    public List<Inventory> getInventory(FilterParams filterParams) {
        List<Inventory> inventoryList;
        Handle h = null;
        try {
            String query = filterParams.buildQuery();
            LOG.info("Query -> " + query);
            h = getDbi().open();
            LOG.debug("inside getINventory with filtering.");

            inventoryList = h.createQuery(query).map(new AddGetInventoryMapper()).list();
            LOG.debug("Got inventory : " + inventoryList.toString());

        } finally {
            if (h != null) {
                h.close();
            }
        }

        return inventoryList;
    }

}

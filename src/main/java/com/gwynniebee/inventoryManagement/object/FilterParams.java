/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.object;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ipsita
 */
public class FilterParams {
    /**
     * criteria is for filtering.
     */
    private Map<String, String> criteria = new HashMap<String, String>();
    /**
     * fields name.
     */
    private List<String> fields;
    /**
     * table name.
     */
    private String table;

    /**
     * @return the filtering criteria
     */
    public Map<String, String> getCriteria() {
        return criteria;
    }

    /**
     * @param criteria Map<String, String>
     */
    public void setCriteria(Map<String, String> criteria) {
        this.criteria = criteria;
    }

    /**
     * @return the list of fields
     */
    public List<String> getFields() {
        return fields;
    }

    /**
     * @param fields List<String>
     */
    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    /**
     * @return the table name
     */
    public String getTable() {
        return table;
    }

    /**
     * @param table String
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * @return the criteria itemId's value
     */
    public String getItemIdfromCriteria() {
        return criteria.get("itemId");
    }

    /**
     * filtering elements.
     */
    public FilterParams() {

        fields = Arrays.asList("*");
    }

    /**
     * @param fields List<String>
     */
    public FilterParams(List<String> fields) {
        super();
        this.fields = fields;
    }

    /**
     * @param fields List<String>
     * @param table String
     */
    public FilterParams(List<String> fields, String table) {
        super();
        this.fields = fields;
        this.table = table;
    }

    /**
     * add the key value pair in Criteria map.
     * @param key String
     * @param value String
     */
    public void addCriteria(String key, String value) {
        criteria.put(key, value);
    }

    /**
     * build the dynamic query.
     * @return the genrated query
     */
    public String buildQuery() {
        StringBuilder query = new StringBuilder();
        query.append("select ");
        int i = 1;
        for (String field : fields) {
            query.append(field);
            if (i < fields.size()) {
                query.append(", ");
            }
            i++;

        }

        query.append(" FROM ");
        query.append(table);

        if (!criteria.containsKey("status")) {
            criteria.put("status<", "2");
        }

        String limit = null;
        String offset = null;

        if (criteria.containsKey("limit")) {
            limit = criteria.get("limit");
            criteria.remove("limit");
        }

        if (criteria.containsKey("offset")) {
            offset = criteria.get("offset");
            criteria.remove("offset");
        }

        if (!criteria.isEmpty()) {
            query.append(" WHERE ");
        }

        i = 1;

        for (String field : criteria.keySet()) {

            query.append(field);
            query.append("=");
            query.append(criteria.get(field));
            if (i < criteria.size()) {
                query.append(" AND ");
            }
            i++;
        }

        if (limit != null) {
            query.append(" LIMIT " + limit);
        }
        if (offset != null) {
            query.append(" OFFSET " + offset);
        }

        return query.toString();
    }

}

/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.object;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;

import com.gwynniebee.rest.common.jackson.JodaDateTimeDeserializer;
import com.gwynniebee.rest.common.jackson.JodaDateTimeSerializer;

/**
 * @author ipsita
 */
public class Transections {

    private String employeeId;
    private DateTime expectedReturn;
    private String issuedBy;
    private DateTime issuedOn;
    private int itemId;
    private int namespaceId;
    private DateTime returnedOn;
    private String returnTo;
    private int status;
    private int transactionId;

    /**
     * @return the transection id
     */
    public int getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId int
     */
    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * @param itemId int
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     * @return the meployeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId String
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return namspaceId
     */
    public int getNamespaceId() {
        return namespaceId;
    }

    /**
     * @param namespaceId int
     */
    public void setNamespaceId(int namespaceId) {
        this.namespaceId = namespaceId;
    }

    /**
     * @return status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status int
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return name of the admin who issued the item
     */
    public String getIssuedBy() {
        return issuedBy;
    }

    /**
     * @param issuedBy String
     */
    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    /**
     * @return date when the item is issued
     */
    public DateTime getIssuedOn() {
        return issuedOn;
    }

    /**
     * @param issuedOn DateTime
     */
    public void setIssuedOn(DateTime issuedOn) {
        this.issuedOn = issuedOn;
    }

    /**
     * @return expected return date
     */
    @JsonSerialize(using = JodaDateTimeSerializer.class)
    public DateTime getExpectedReturn() {
        return expectedReturn;
    }

    /**
     * @param expectedReturn DateTime
     */
    @JsonDeserialize(using = JodaDateTimeDeserializer.class)
    public void setExpectedReturn(DateTime expectedReturn) {
        this.expectedReturn = expectedReturn;
    }

    /**
     * @return return date
     */
    public DateTime getReturnedOn() {
        return returnedOn;
    }

    /**
     * @param returnedOn DateTime
     */
    public void setReturnedOn(DateTime returnedOn) {
        this.returnedOn = returnedOn;
    }

    /**
     * @return name of the admin who got the item back
     */
    public String getReturnTo() {
        return returnTo;
    }

    /**
     * @param returnTo String
     */
    public void setReturnTo(String returnTo) {
        this.returnTo = returnTo;
    }

    /**
     * Transection details.
     * @return transection details in string form
     */
    public String toString() {
        return "Transaction " + this.itemId + " : " + this.employeeId + " : " + this.returnedOn;
    }

}

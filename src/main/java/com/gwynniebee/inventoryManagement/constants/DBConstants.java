/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryManagement.constants;

/**
 * Database constants.
 * @author Ipsita
 */
public final class DBConstants {

    public static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DATABASE_INVENTORY_URL = "jdbc:mysql://induction-dev-t3.gwynniebee.com/t3";
    public static final String DATABASE_USERNAME = "write_all_bi";
    public static final String DATABASE_PASSWORD = "write_all_bi";

    /**
     * default constructor.
     */
    private DBConstants() {

    }

}
